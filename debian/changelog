q2-fragment-insertion (2024.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Autopkgtest for all supported Python3 versions
  * Regenerate debian/control from debian/control.in (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2024 15:01:28 +0100

q2-fragment-insertion (2023.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Generate debian/control automatically to refresh version number
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Regenerate debian/control from debian/control.in (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 30 Jan 2024 17:42:16 +0100

q2-fragment-insertion (2023.7.0-2) UNRELEASED; urgency=medium

  * d/salsa-ci.yml: skip test on i386.

 -- Étienne Mollier <emollier@debian.org>  Sun, 20 Aug 2023 16:31:58 +0200

q2-fragment-insertion (2023.7.0-1) unstable; urgency=medium

  * New upstream version 2023.7.0
  * d/copyright: bump copyright year.
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Sun, 20 Aug 2023 16:09:03 +0200

q2-fragment-insertion (2022.11.1-3) unstable; urgency=medium

  * Team upload.
  * Do not Build-Depend sepp

 -- Andreas Tille <tille@debian.org>  Sun, 05 Feb 2023 18:26:51 +0100

q2-fragment-insertion (2022.11.1-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Andreas Tille <tille@debian.org>  Tue, 24 Jan 2023 18:50:58 +0100

q2-fragment-insertion (2022.11.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Bump versioned Depends
  * Standards-Version: 4.6.2 (routine-update)
  * Skip autopkgtest requiring sepp which needs pplacer which is not
    available in testing since mcl lost OCaml support
  * DEP3

 -- Andreas Tille <tille@debian.org>  Tue, 17 Jan 2023 11:36:48 +0100

q2-fragment-insertion (2022.8.0-2) unstable; urgency=medium

  * Team upload.
  * Architecture: any (to avoid debci errors on architectures where sepp
    is not available ... which are all except amd64)

 -- Andreas Tille <tille@debian.org>  Fri, 30 Sep 2022 08:17:13 +0200

q2-fragment-insertion (2022.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2022.8.0
  * Update dependency on qiime/q2-* >= 2022.8.0

 -- Mohammed Bilal <mdbilal@disroot.org>  Wed, 07 Sep 2022 12:25:31 +0000

q2-fragment-insertion (2022.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Adjust versioned (Build-)Depends

 -- Andreas Tille <tille@debian.org>  Tue, 19 Jul 2022 17:52:24 +0200

q2-fragment-insertion (2021.8.0-3) unstable; urgency=medium

  * Initial release (Closes: #977115)
  * Reupload after adding copyright info for versioneer.

 -- Steffen Moeller <moeller@debian.org>  Sun, 26 Sep 2021 20:57:50 +0200
